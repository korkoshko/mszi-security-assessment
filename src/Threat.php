<?php

namespace korkoshko;

class Threat
{
    /**
     * @var string
     */
    protected string $name;

    /**
     * @var array
     */
    protected array $probabilities;

    /**
     * Threat constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->probabilities = [];
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Probability $probability
     *
     * @return Threat
     */
    public function addProbability(Probability $probability)
    {
        array_push($this->probabilities, $probability);
        return $this;
    }

    /**
     * @param int $index
     *
     * @return mixed|null
     */
    public function getProbability(int $index)
    {
        return $this->probabilities[$index] ?? null;
    }
}