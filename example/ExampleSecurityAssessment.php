<?php

require_once __DIR__ . '/../vendor/autoload.php';

use korkoshko\{
    SecurityAssessment,
    FuzzyNumber,
    Probability,
    ThreatResource,
    Threat
};

$resources = [
    'Файли користувачів'  => 0.1,
    'Навчальні матеріали' => 0.3,
    'Питання тестів'      => 0.5,
    'Системні файлі'      => 0.1,
];

$threats = [
    'Людський фактор'                     => [
        0.09, 0.08, 0.1, 0,
    ],
    'Некорректне налаштування мережі'     => [
        0.05, 0.04, 0.07, 0.07,
    ],
    'Некорректне розмеження прав доступу' => [
        0.1, 0.08, 0.15, 0.05,
    ],
    'Збій в програмному забезпеченні'     => [
        0.02, 0.02, 0.17, 0.02,
    ],
    'Шпигунське  програмне забезпечення'  => [
        0.04, 0.15, 0.15, 0.04,
    ],
    'Підслуховування'                     => [
        0.03, 0.03, 0.04, 0,
    ],
    'Несанкціонований доступ'             => [
        0.1, 0.1, 0.12, 0.08,
    ],
    'Соціальна інженерія'                 => [
        0.01, 0.02, 0.1, 0,
    ],
    'Застаріле  програмне забезпечення'   => [
        0.08, 0.09, 0.1, 0.03,
    ],
    'Відсутність контролю доступу'        => [
        0.3, 0.38, 0.2, 0.12,
    ],
];

$assessment = new SecurityAssessment();

foreach ($resources as $resource => $probability) {
    $probability = new Probability($probability);

    $assessment->addResource(
        new ThreatResource($resource, $probability)
    );
}

foreach ($threats as $threat => $probabilities) {
    $threat = new Threat($threat);

    foreach ($probabilities as $index => $probability) {
        $fuzzyNumber = new FuzzyNumber($probability);

        $threat->addProbability(
            new Probability($fuzzyNumber, $index)
        );
    }

    $assessment->addThreat($threat);
}

$threats = $assessment->getThreats();

foreach ($assessment->calcWithRanks() as $place => $rank) {
    $threatIndex = $rank->getThreatIndex();
    $threatName = $threats[$threatIndex]->getName();
    $threatProbability = $rank->getFuzzyNumber()->getBase();

    echo '#' . ($place + 1) . ' ' . $threatName . ': ' . $threatProbability . PHP_EOL;
}
