<?php

namespace korkoshko;

class SecurityAssessment
{
    /**
     * @var array
     */
    protected array $resources;

    /**
     * @var array
     */
    protected array $threats;

    /**
     * Matrix constructor.
     */
    public function __construct()
    {
        $this->resources = [];
        $this->threats = [];
    }

    /**
     * @param Threat $threat
     *
     * @return $this
     */

    public function addThreat(Threat $threat): self
    {
        array_push($this->threats, $threat);
        return $this;
    }

    /**
     * @param ThreatResource $resource
     *
     * @return $this
     */
    public function addResource(ThreatResource $resource): self
    {
        array_push($this->resources, $resource);
        return $this;
    }


    /**
     * @return Probability
     */
    public function calc(): Probability
    {
        return $this->calculation($this->threats);
    }

    /**
     * @param int              $indexThreat
     *
     * @param Probability|null $probabilityAttack
     *
     * @return Probability
     */
    public function calcWithoutTreat(int $indexThreat, ?Probability $probabilityAttack = null): Probability
    {
        $threats = $this->threats;

        unset($threats[$indexThreat]);

        $probabilityAttack = ($probabilityAttack ?? $this->calc())->getFuzzyNumber();
        $probabilityWithoutTreat = $this->calculation($threats)->getFuzzyNumber();

        return new Probability(
            $probabilityAttack->subtract($probabilityWithoutTreat),
            $indexThreat
        );
    }

    /**
     * @return array
     */
    public function calcWithRanks(): array
    {
        $ranks = [];
        $probabilityAttack = $this->calc();

        foreach ($this->threats as $index => $threat) {
            $ranks[] = $this->calcWithoutTreat($index, $probabilityAttack);
        }

        return $this->sort($ranks);
    }

    /**
     * @param array $threats
     *
     * @return Probability
     */
    protected function calculation(array $threats): Probability
    {
        $result = new FuzzyNumber(0.0);

        foreach ($this->resources as $index => $resource) {
            $composition = $this->getCompositionProbabilities($threats, $index);
            $resourceProbability = $resource->getProbabilityAttack();

            $result = $result->add(
                $resourceProbability->getFuzzyNumber()->multiply(
                    $composition->getReverse()
                )
            );
        }

        return new Probability(
            $result
        );
    }

    /**
     * @return array
     */
    public function getThreats(): array
    {
        return $this->threats;
    }

    /**
     * @return array
     */

    public function getResources(): array
    {
        return $this->resources;
    }

    /**
     * @param array $threats
     * @param int   $resourceIndex
     *
     * @return Probability
     */
    protected function getCompositionProbabilities(array $threats, int $resourceIndex): Probability
    {
        $composition = new FuzzyNumber(1.0);

        foreach ($threats as $threatIndex => $threat) {
            $composition = $composition->multiply(
                $threat->getProbability($resourceIndex)->getReverse()
            );
        }

        return new Probability(
            $composition,
        );
    }

    /**
     * @param array $array
     *
     * @return array
     */
    private function sort(array $array)
    {
        usort($array,
            fn($current, $next) => $next->getFuzzyNumber()->getBase() > $current->getFuzzyNumber()->getBase()
        );

        return $array;
    }
}