<?php

namespace korkoshko;

class FuzzyNumber
{

    /**
     * @var float
     */
    protected float $base;

    /**
     * @var float
     */
    protected float $left;

    /**
     * @var float
     */
    protected float $right;

    /**
     * FuzzyNumber constructor.
     *
     * @param float $base
     * @param float $left
     * @param float $right
     */
    public function __construct(float $base, $left = 0.0, $right = 0.0)
    {
        $this->base = $base;
        $this->left = $left;
        $this->right = $right;
    }

    /**
     * @param FuzzyNumber $target
     *
     * @return FuzzyNumber
     */
    public function add(self $target)
    {
        return new static(
            $this->base + $target->getBase(),
            $this->left + $target->getLeft(),
            $this->right + $target->getRight(),
        );
    }

    /**
     * @param FuzzyNumber $target
     *
     * @return FuzzyNumber
     */
    public function subtract(self $target)
    {
        return new static(
            $this->base - $target->getBase(),
            $this->left + $target->getLeft(),
            $this->right + $target->getRight(),
        );
    }

    /**
     * @param FuzzyNumber $target
     *
     * @return FuzzyNumber
     */
    public function multiply(self $target)
    {
        return new static(
            $this->base * $target->getBase(),
            $this->base * $target->getLeft() + $target->getBase() * $this->left,
            $this->base * $target->getRight() + $target->getBase() * $this->right,
        );
    }

    /**
     * @return float
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @return float
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * @return float
     */
    public function getRight()
    {
        return $this->right;
    }
}