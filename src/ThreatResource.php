<?php

namespace korkoshko;

class ThreatResource
{
    /**
     * @var string
     */
    protected string $name;

    /**
     * @var Probability
     */
    protected Probability $probabilityAttack;

    /**
     * ThreatResource constructor.
     *
     * @param string      $name
     * @param Probability $probabilityAttack
     */
    public function __construct(string $name, Probability $probabilityAttack)
    {
        $this->name = $name;
        $this->probabilityAttack = $probabilityAttack;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param Probability $probability
     *
     * @return $this
     */
    public function setProbabilityAttack(Probability $probability)
    {
        $this->probabilityAttack = $probability;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Probability
     */
    public function getProbabilityAttack()
    {
        return $this->probabilityAttack;
    }
}