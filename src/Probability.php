<?php

namespace korkoshko;

class Probability
{
    /**
     * @var int
     */
    protected ?int $threatIndex;

    /**
     * @var FuzzyNumber
     */
    protected FuzzyNumber $fuzzyNumber;

    /**
     * Probability constructor.
     *
     * @param mixed    $number
     * @param int|null $threatIndex
     */
    public function __construct($number, ?int $threatIndex = null)
    {
        $this->fuzzyNumber = is_numeric($number) ? new FuzzyNumber($number) : $number;
        $this->threatIndex = $threatIndex;
    }

    public function setFuzzyNumber(FuzzyNumber $fuzzyNumber)
    {
        $this->fuzzyNumber = $fuzzyNumber;
        return $this;
    }

    /**
     * @param int $index
     *
     * @return $this
     */
    public function setThreatIndex(int $index)
    {
        $this->threatIndex = $index;
        return $this;
    }

    /**
     * @return FuzzyNumber
     */
    public function getFuzzyNumber()
    {
        return $this->fuzzyNumber;
    }

    /**
     * @return int|null
     */
    public function getThreatIndex()
    {
        return $this->threatIndex;
    }

    /**
     * @return FuzzyNumber
     */
    public function getReverse()
    {
        $fuzzyNumber = new FuzzyNumber(1.0);
        return $fuzzyNumber->subtract($this->getFuzzyNumber());
    }
}